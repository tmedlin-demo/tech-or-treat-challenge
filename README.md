# Happy Architecture Day! We are so excited to meet you!  

## Getting started

Welcome to the GitLab Code Challenge! `Were so glad your here`. Hopefully you aren't too spooked by spelling errors and YAML! 

If you do not already have a [GitLab.com](gitlab.com) account, please sign up [here](https://gitlab.com/users/sign_up) so you can participate.
To view this repo on your own machine, you can use the shortlink [https://bit.ly/arch-days](https://bit.ly/arch-days)

## Prework 

To get started, you'll need to create a [GitLab.com](gitlab.com)  account. Once you've created your account, please go to [GitLabDemo.com](gitlabdemo.com) and redeem the following code: **c48ebf8c**

You will need to enter the redemtiopn code, and your GitLab.com username (double check this!) to redeem your Ultimate group. Please ask questions at the GitLab booth if you run into any roadblocks. 

## Level 1

Simply fork this project into `you're` test `gorup` to get `starter`. 

Once that is `don't`, fix the `speeling` errors or other typos in this section of the `READYOU.md`. Then make a [merge request](https://gitlab.com/tmedlin-demo/architecture-day-challenge/-/merge_requests) to this repository.

Comment on your new `pull` request saying "@taylor - Please review!" Please make sure you are tagging Taylor Carr. 

## Level 2

For level two, check out our [Static Application Security Testing (SAST) Documentation](https://docs.gitlab.com/ee/user/application_security/sast/). Use the documentation to manually add the SAST scanner to your forked project's gitlab-ci.yml file. 

Hint: you can test out the pipeline editor to validate your syntax! This will be under Build -> Pipeline Editor. 

Hint Hint: If you are having trouble finding the correct include statement, try asking GitLab Duo Chat! The top right corner of your window (while looking at a repo) will have a button to access our chat assistant. 

## Level 4 

For level 4, let's go ahead and undo the work we just did! Please swap the SAST template you used in for the new [SAST Component](https://docs.gitlab.com/ee/ci/components/) from our [CI/CD Catalog](https://gitlab.com/explore/catalog). To make things slightly more interesting, you can change the stage that the SAST job will run in using the inputs defined in the SAST Component. 

When you've made your changes, please open a new merge request and tag @taylor. 

Hint: The CI file in this project is pretty barren; you may need to add some more stages to it. Please swing by the booth if you need help! 

## Bonus Level!!

For the bonus level, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to: The Ruby on Rails backend, the Vue-based frontend, the Go-based services like the GitLab Runner and Gitaly, and the documentation for all of those things and more.
